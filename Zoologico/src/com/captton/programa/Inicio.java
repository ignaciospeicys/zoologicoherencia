package com.captton.programa;

import com.captton.zoologico.*;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Perro perro1 = new Perro("Gumby",15,30);
		Pez pez1 = new Pez("Aletas",5,2);
		Pajaro pajaro1 = new Pajaro("Alcon",20,15);
		
		System.out.println("Info sobre Pez: "+pez1.getNombre());
		System.out.println("Edad: "+pez1.getEdad() +" a�os");
		System.out.println("Peso: "+pez1.getPeso() +"kg");
		
		System.out.println("\nPez come 1kg de alimento");
		pez1.IngerirAlimento(1);
		System.out.println("Nuevo Peso: "+pez1.getPeso() +"kg");
		
		System.out.println("\nPez Nada un rato...");
		System.out.println(pez1.Nadar());
		System.out.println("Nueva Edad: "+pez1.getEdad() +" a�os");
		
		System.out.println("\nInfo sobre Perro: "+perro1.getNombre());
		System.out.println("Edad: "+perro1.getEdad() +" a�os");
		System.out.println("Peso: "+perro1.getPeso() +"kg");
		
		System.out.println("\nPerro come 2kg de alimento");
		perro1.IngerirAlimento(2);
		System.out.println("Nuevo Peso: "+perro1.getPeso() +"kg");
		
		System.out.println("\nPerro Ladra un rato...");
		System.out.println(perro1.Ladrar());
		System.out.println("Nueva Edad: "+perro1.getEdad() +" a�os");
		
		System.out.println("\nInfo sobre Pajaro: "+pajaro1.getNombre());
		System.out.println("Edad: "+pajaro1.getEdad() +" a�os");
		System.out.println("Peso: "+pajaro1.getPeso() +"kg");
		
		System.out.println("\nPajaro come 2kg de alimento");
		pajaro1.IngerirAlimento(2);
		System.out.println("Nuevo Peso: "+pajaro1.getPeso() +"kg");
		
		System.out.println("\nPajaro Ladra un rato...");
		System.out.println(pajaro1.Volar());
		System.out.println("Nueva Edad: "+pajaro1.getEdad() +" a�os");		

	}

}
